#!/bin/sh

. $(dirname "${0}")/set-pax-flag-of-java.sh

# the functions require GNU sed (the usage of busybox sed may result into incorrect outputs)

addZeppelinProperty() {
	local path="${1}"
	local name="${2}"
	local value="${3}"
	local entry="<property><name>${name}</name><value>${value}</value></property>"
	local escapedEntry=$(echo "${entry}" | sed 's/\//\\\//g')
	sed -i "/<\/configuration>/ s/.*/${escapedEntry}\n&/" "${path}"
}

cleanZeppelinProperties() {
	local path="${1}"
	sed -i '/^<configuration>/,/^<\/configuration>/{//!d}' "${path}"
}

reconfZeppelinByEnvVars() {
	local path="${1}"
	local envPrefix="${2}"
	[ -e "${path}" ] || cp "${path}.template" "${path}"
	echo "* reconfiguring ${path}"
	cleanZeppelinProperties "${path}"
	for I in `printenv | grep "^${envPrefix}_[^=]*="`; do
		local name=`echo "${I}" | sed -e "s/^${envPrefix}_\\([^=]*\\)=.*\$/\\1/" -e 's/___/-/g' -e 's/__/_/g' -e 's/_/./g'`
		local value="${I#*=}"
		echo "** setting ${name}=${value}"
		addZeppelinProperty "${path}" "${name}" "${value}"
	done
}

reconfZeppelinByEnvVars "${ZEPPELIN_SITE_CONF}" PROP_ZEPPELIN
